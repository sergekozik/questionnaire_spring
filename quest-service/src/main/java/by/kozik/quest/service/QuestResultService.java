package by.kozik.quest.service;

import by.kozik.quest.bean.AnswerDoubleResultBean;
import by.kozik.quest.bean.QuestionFormBean;
import by.kozik.quest.bean.UserQuestResult;

import java.util.List;

/**
 * Created by Serge_Kozik on 3/13/2017.
 */
public interface QuestResultService {

    public List<AnswerDoubleResultBean> returnProcentChart(QuestionFormBean question);
    public double calculateTotalMark(int questId);
    public double calculateUserMark(int mainResultId);
    public UserQuestResult saveMainResult(UserQuestResult questResult);
}
