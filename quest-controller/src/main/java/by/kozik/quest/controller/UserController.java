package by.kozik.quest.controller;

import by.kozik.quest.bean.*;
import by.kozik.quest.exception.BeanCreateException;
import by.kozik.quest.exception.MissingParameterInSessionException;
import by.kozik.quest.exception.SaveBeanException;
import by.kozik.quest.exception.WrongRequestParameterException;
import by.kozik.quest.service.QuestResultService;
import by.kozik.quest.service.QuestService;
import by.kozik.quest.service.QuestTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private QuestService questService;

    @Autowired
    private QuestResultService questResultService;

    @Autowired
    private MessageSource messageSource;

    private ModelAndView prepareQuestionPage(HttpServletRequest request) throws MissingParameterInSessionException {
        HttpSession session = request.getSession();
        Object questObj = session.getAttribute("startQuest");
        Object iteratorObj = session.getAttribute("questionsIterator");
        Object resultsObj = session.getAttribute("currentResult");
        if ((questObj==null)||(iteratorObj==null)||(resultsObj==null)) {
            throw new MissingParameterInSessionException("Session with current quest has been expired.");
        }
        Iterator<QuestionFormBean> questionFormBeanIterator = (Iterator<QuestionFormBean>) iteratorObj;
        QuestionFormBean questionBean = questionFormBeanIterator.next();
        ModelAndView mav = new ModelAndView();
        if (questionBean!=null) {
            mav.setViewName("ask-question.page");
            session.setAttribute("question_id",questionBean.getId());
            session.setAttribute("question_formulation",questionBean.getFormulation());
            List<AnswerParentBean> choiceAnswers = new ArrayList<>();
            List<AnswerParentBean> userAnswers = new ArrayList<>();
            for (AnswerParentBean answer:questionBean.getAnswers()) {
                if (answer.getClass()== AnswerUserTextBean.class) {
                    userAnswers.add(answer);
                } else {
                    choiceAnswers.add(answer);
                }
            }
            session.setAttribute("question_variants",choiceAnswers);
            session.setAttribute("question_variants_user",userAnswers);
            return mav;
        } else {
            session.setAttribute("question_id",null);
            session.setAttribute("question_formulation",null);
            session.setAttribute("question_variants",null);
            session.setAttribute("question_variants_user",null);
            return new ModelAndView("quest-finish.html");
        }
    }

    private boolean validateQuestionPage(HttpServletRequest request, Locale locale) throws MissingParameterInSessionException,WrongRequestParameterException {
        String questionIdString = request.getParameter("question_id");
        if (questionIdString==null) {
            throw new MissingParameterInSessionException("question id was not found.");
        }
        int questionId = Integer.parseInt(questionIdString);
        HttpSession session = request.getSession();
        Object questObj = session.getAttribute("startQuest");
        if (questObj==null) {
            throw new MissingParameterInSessionException("Session with current quest has been expired.");
        }
        QuestShowBean quest = (QuestShowBean) questObj;
        int questId = quest.getId();
        boolean flagExactQuest = false;
        for (QuestionFormBean question:quest.getQuestions()) {
            if (question.getId()==questionId) {
                flagExactQuest=true;
                break;
            }
        }
        if (!flagExactQuest) {
            throw new MissingParameterInSessionException("Session with current quest has been expired.");
        }
        String[] textAnswers = request.getParameterValues("current_answer_text");
        String[] userAnswers = request.getParameterValues("user_answer");
        String[] choiceAnswers = request.getParameterValues("current_answer");
        if ((textAnswers==null)&&(choiceAnswers==null)) {
            request.setAttribute("error_question_message",messageSource.getMessage("message.label.error.question-answers-notfound",null,locale));
            return false;
        }
        if ((textAnswers!=null)&&((userAnswers==null)||(textAnswers.length>userAnswers.length))) {
            throw new WrongRequestParameterException("User did not answered for all checked answer cases.");
        }
        return true;
    }

    @RequestMapping("quest-start.html")
    public ModelAndView doStartQuest(@RequestParam(value = "quest_id", required = false) Integer questId,
                                     HttpServletRequest request)
            throws BeanCreateException
    {
        if (questId==null) {
            return new ModelAndView("redirect:/main.html");
        }
        QuestShowBean questShowBean = questService.returnQuestById(questId);
        if (questShowBean==null) {
            throw new BeanCreateException("Quest bean was not created for id="+questId);
        }
        ModelAndView mav = new ModelAndView("quest-before-start.page");
        HttpSession session = request.getSession();
        session.setAttribute("startQuest",questShowBean);
        session.setAttribute("questionsIterator",questShowBean.getQuestions().iterator());
        session.setAttribute("currentResult", new UserQuestResult());
        mav.addObject("quest_title",questShowBean.getTitle());
        mav.addObject("quest_description",questShowBean.getDescription());
        return mav;
    }

    @RequestMapping("quest-run.html")
    public ModelAndView doRunQuest(HttpServletRequest request) throws MissingParameterInSessionException {
        return prepareQuestionPage(request);
    }

    @RequestMapping("quest-next-question.html")
    public ModelAndView doNextQuestion(HttpServletRequest request, Locale locale) throws MissingParameterInSessionException, WrongRequestParameterException {

        if (!validateQuestionPage(request,locale)) {
            return new ModelAndView("ask-question.page");
        }
        List<UserAnswerResult> currentAnswers = new ArrayList<>();
        String[] choiceAnswers = request.getParameterValues("current_answer");
        if (choiceAnswers!=null) {
            for (String chosenAnswerId:choiceAnswers) {
                currentAnswers.add(new UserAnswerResult(Integer.parseInt(chosenAnswerId)));
            }
        }
        String[] textAnswers = request.getParameterValues("current_answer_text");
        String[] userAnswers = request.getParameterValues("user_answer");
        if (textAnswers!=null) {
            for (int ii=0;ii<textAnswers.length; ii++) {
                currentAnswers.add(new UserAnswerResultText(Integer.parseInt(textAnswers[ii]),userAnswers[ii]));
            }
        }
        HttpSession session = request.getSession();
        Object resultsObj = session.getAttribute("currentResult");
        UserQuestResult userQuestResult = (UserQuestResult) resultsObj;
        userQuestResult.getAnswers().addAll(currentAnswers);
        return prepareQuestionPage(request);
    }

    @RequestMapping("quest-finish.html")
    public ModelAndView doQuestFinish(HttpServletRequest request) throws MissingParameterInSessionException, SaveBeanException, WrongRequestParameterException {
        HttpSession session = request.getSession();
        Object questObj = session.getAttribute("startQuest");
        Object resultsObj = session.getAttribute("currentResult");
        if ((questObj==null)||(resultsObj==null)) {
            throw new MissingParameterInSessionException("Session with current quest has been expired.");
        }
        ModelAndView mav = new ModelAndView();
        QuestShowBean questShowBean = (QuestShowBean)questObj;
        UserQuestResult userQuestResult = (UserQuestResult)resultsObj;
        if (userQuestResult.getQuestId()!=questShowBean.getId()) {
            throw new WrongRequestParameterException("Current results do not match started quest.");
        }
        UserQuestResult savedResult = questResultService.saveMainResult(userQuestResult);
        if (savedResult==null) {
            throw new SaveBeanException("Error while saving results to database.");
        }
        mav.addObject("quest_title",questShowBean.getTitle());
        if (QuestTypeEnum.VOTING.getNameEn().equals(questShowBean.getTypeResultNative())) {
            QuestionFormBean questionBean = questShowBean.getQuestions().get(0);
            List<AnswerDoubleResultBean> voteResults = questResultService.returnProcentChart(questionBean);
            mav.addObject("voteResultBeans",voteResults);
            mav.setViewName("quest-result-voting.page");
        }
        if (QuestTypeEnum.QUESTIONNAIRE.getNameEn().equals(questShowBean.getTypeResultNative())) {
            mav.setViewName("quest-result-questionnaire.page");
            mav.addObject("quest_num_questions",questShowBean.getQuestions().size());
        }
        if (QuestTypeEnum.TEST_MARK.getNameEn().equals(questShowBean.getTypeResultNative())) {
            mav.setViewName("quest-result-test.page");
            mav.addObject("quest_num_questions",questShowBean.getQuestions().size());
            mav.addObject("quest_score", questResultService.calculateUserMark(savedResult.getId()));
            mav.addObject("quest_total",questResultService.calculateTotalMark(questShowBean.getId()));
        }
        session.setAttribute("startQuest",null);
        session.setAttribute("questionsIterator",null);
        session.setAttribute("currentResult", null);
        return mav;
    }
}
