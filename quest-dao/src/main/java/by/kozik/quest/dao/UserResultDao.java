package by.kozik.quest.dao;

import by.kozik.quest.entity.UserMainResultEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Serge_Kozik on 3/13/2017.
 */
@Repository
public interface UserResultDao extends PagingAndSortingRepository<UserMainResultEntity,Integer> {

}
