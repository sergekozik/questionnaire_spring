package by.kozik.quest.exception;

/**
 * Created by Serge_Kozik on 3/13/2017.
 */
public class WrongRequestParameterException extends Exception {

    public WrongRequestParameterException() {
    }

    public WrongRequestParameterException(String message) {
        super(message);
    }

    public WrongRequestParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongRequestParameterException(Throwable cause) {
        super(cause);
    }

    public WrongRequestParameterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
